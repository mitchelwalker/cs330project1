def collatz_eval(num):
    cycle = 0
    temp = num
    while True:
        if temp == 1:
            return 1
        elif temp % 2 == 0:
            temp = temp/2
            cycle += 1
            continue
        else:
            temp = 3*temp + 1
            cycle +=1
    return True
